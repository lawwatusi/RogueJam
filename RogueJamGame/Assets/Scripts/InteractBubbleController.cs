using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBubbleController: MonoBehaviour {
  [SerializeField] SpriteRenderer _spriteRenderer;
  [SerializeField] float _cyclesPerSecond = 1f;
  [SerializeField] float _yRange = 0.125f;

  float _initialY;

  void Awake() {
    _initialY = transform.localPosition.y;
  }

  void Update() {
    if (!_spriteRenderer.enabled) {
      return;
    }

    float t = Time.realtimeSinceStartup;
    Vector3 localPosition = transform.localPosition;
    float halfYRange = _yRange / 2f;
    float twoPi = 2f * Mathf.PI;
    float y = _initialY + halfYRange * Mathf.Sin((t * twoPi * _cyclesPerSecond) % twoPi);
    transform.localPosition = new Vector3(localPosition.x, y, localPosition.z);
  }

  public void Show() {
    _spriteRenderer.enabled = true;
  }

  public void Hide() {
    _spriteRenderer.enabled = false;
  }
}
