using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GhostController: MonoBehaviour {
  public GameObject _target;

  [SerializeField] float _health = 3f;
  [SerializeField] float _speed = 1f;

  [SerializeField] GameObject _ghostPissPrefab;
  [SerializeField] Sprite[] _ghostSprites;
  [SerializeField] float _faceChangeCooldown = 1f;

  [SerializeField] Rigidbody2D _rigidbody2D;
  [SerializeField] SpriteRenderer _spriteRenderer;

  [SerializeField] float _wiggleSpeed = 1f;
  [SerializeField] float _wiggleAmount = 0.2f;

  Color _initialColor;
  float _faceChangeCooldownTimer;
  float _hitTimer = 0f;

  void Awake() {
    _faceChangeCooldownTimer = _faceChangeCooldown;
    _initialColor = _spriteRenderer.color;
  }

  void Update() {
    if (_hitTimer > 0f) {
      _hitTimer -= Time.deltaTime;
      if (_health <= 0f) {
        _spriteRenderer.color = new Color(1f, 1f, 1f, _hitTimer / 1f);
      }
      else if (_hitTimer <= 0f) {
        _spriteRenderer.color = _initialColor;
      }
      return;
    }

    if (_health <= 0f) {
      Destroy(gameObject);
    }

    ChaseTarget();
    UpdateWiggle();
    UpdateFace();
    UpdateTrail();
  }

  void ChaseTarget() {
    var dt = Time.deltaTime;
    var pos = transform.position;
    var targetPos = _target.transform.position;
    var posDelta = targetPos - pos;
    _rigidbody2D.velocity = posDelta.normalized * _speed;

    var xDiff = targetPos.x - pos.x;
    if (xDiff >= 0.25f) {
      _spriteRenderer.flipX = false;
    }
    else if (xDiff <= -0.25f) {
      _spriteRenderer.flipX = true;
    }
  }

  void UpdateWiggle() {
    float t = Time.realtimeSinceStartup;
    float twoPi = 2f * Mathf.PI;
    float wigglePhase = (t * twoPi * _wiggleSpeed) % twoPi;
    float yScale = 1f + Mathf.Sin(wigglePhase) * _wiggleAmount;
    _spriteRenderer.gameObject.transform.localScale = new Vector3(1f, yScale, 1f);
  }

  void UpdateFace() {
    if (_faceChangeCooldownTimer > 0f) {
      _faceChangeCooldownTimer -= Time.deltaTime;
    }
    float chance = Random.Range(0f, 1f);
    if (chance < (0.01f - (_faceChangeCooldownTimer * 0.01f))) {
      int nextFaceIndex = Random.Range(0, 4);
      _spriteRenderer.sprite = _ghostSprites[nextFaceIndex];
      _faceChangeCooldownTimer = _faceChangeCooldown;
    }
  }

  void UpdateTrail() {
    float chance = Random.Range(0f, 1f);
    if (chance < 0.0005f) {
      var newGhostPiss = Instantiate(_ghostPissPrefab, transform.position + Vector3.up * 0.25f, Quaternion.identity);
      newGhostPiss.transform.localScale = new Vector3(Random.Range(1, 3), 1f, 1f);
      var ghostPissSpriteRenderer = newGhostPiss.GetComponent<SpriteRenderer>();
      ghostPissSpriteRenderer.color = new Color(1f, 1f, 1f, Random.Range(0.1f, 0.3f));
    }
  }

  void OnTriggerEnter2D(Collider2D other) {
    if (_hitTimer <= 0f && other.gameObject.CompareTag("Damage")) {
      _health -= 1f;
      _hitTimer = 1f;
      _rigidbody2D.velocity = Vector2.zero;
      _spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
    }
  }
}
