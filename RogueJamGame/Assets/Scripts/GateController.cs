using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateController: MonoBehaviour {
  [SerializeField] SpriteRenderer _spriteRenderer;
  [SerializeField] Collider2D _closedCollider;
  [SerializeField] Sprite _closedSprite;
  [SerializeField] Sprite _openSprite;

  bool _isOpen = false;

  public void ToggleState() {
    _isOpen = !_isOpen;
    if (_isOpen) {
      _spriteRenderer.sprite = _openSprite;
      _closedCollider.isTrigger = true;
    }
    else {
      _spriteRenderer.sprite = _closedSprite;
      _closedCollider.isTrigger = false;
    }
  }
}
