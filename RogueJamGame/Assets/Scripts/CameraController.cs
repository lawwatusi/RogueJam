using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController: MonoBehaviour {
  [SerializeField]
  Rigidbody2D _rigidbody2D;

  [SerializeField]
  GameObject _target;

  [SerializeField]
  float _trackSpeed = 5;

  void Update() {
    if (!_target) {
      return;
    }

    Vector3 position3D = transform.position;
    Vector2 targetPos = _target.transform.position;
    var posDelta = targetPos - (Vector2)position3D;
    // idk why this works better than position lerp or velocity lerp lol
    _rigidbody2D.velocity = posDelta * _trackSpeed;
  }
}
