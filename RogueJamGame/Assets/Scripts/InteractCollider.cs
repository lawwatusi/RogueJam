using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractCollider: MonoBehaviour {
  List<Interactable> _interactTargets = new List<Interactable>();
  public List<Interactable> InteractTargets => _interactTargets;

  void OnTriggerEnter2D(Collider2D other) {
    if (other.gameObject.CompareTag("Interactable")) {
      InteractTargets.Add(other.gameObject.GetComponent<Interactable>());
    }
  }

  void OnTriggerExit2D(Collider2D other) {
    if (other.gameObject.CompareTag("Interactable")) {
      InteractTargets.Remove(other.gameObject.GetComponent<Interactable>());
    }
  }
}
