using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverCollider: MonoBehaviour {
  void OnTriggerEnter2D(Collider2D other) {
    if (other.gameObject.CompareTag("Interactable")) {
      var interactableComponent = other.GetComponent<Interactable>();
      interactableComponent.OnHoverStart();
    }
  }

  void OnTriggerExit2D(Collider2D other) {
    if (other.gameObject.CompareTag("Interactable")) {
      var interactableComponent = other.GetComponent<Interactable>();
      interactableComponent.OnHoverEnd();
    }
  }
}
